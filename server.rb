require 'sinatra'
require 'yaml'
require 'json'
require 'fileutils'

deploy_info_path = ".deploy_info.yml"
FileUtils.touch(deploy_info_path)

# env["rack.logger"] = Logger.new("log.log")

get '/' do
	File.read("index.html")
end

get '/repositories' do
	content = File.read("config/build.yml")
	build_config = YAML.load(content)
	build_config['repos'].keys.to_json
end

get '/repositories/:name/branches' do |name|
	content = File.read("config/build.yml")
	build_config = YAML.load(content)
	build_config['repos'][name]['branches'].to_json
end

get '/build_info' do 
	result = {}
	content = File.read("config/build.yml")
	build_config = YAML.load(content)
	repository_names = build_config['repos'].keys
	repository_names.each do |repository_name|
		result[repository_name] = {}
		branch_names = build_config['repos'][repository_name]['branches']
		branch_names.map do |branch_name|
			Dir.chdir("builds/#{repository_name}/#{branch_name}")
			dists = Dir.glob('*')
			result[repository_name][branch_name] = dists
			Dir.chdir('../../..')
		end
	end
	result.to_json
end

get '/repositories/:name/branches/:branch_name/dists' do |name, branch_name|
	Dir.chdir("builds/#{name}/#{branch_name}")
	dists = Dir.glob('*').to_json
	Dir.chdir('../../..')
	dists
end

get '/hosts' do
	content = File.read("config/hosts.yml")
	hosts = YAML.load(content)
	hosts['hosts'].to_json
end

get '/deploy_info' do
	FileUtils.touch(deploy_info_path)
	content = File.read(deploy_info_path)
	deploy_info = if content.size > 0
									YAML.load(content)
								else
									{'hosts' => []}
								end

	deploy_info.to_json
end

def push_deploy_info(message)
  `git add .deploy_info.yml`
  `git commit -m "#{message}"`
  `git push`
end

post '/publish' do
	host = params['host']
	tenant = params['tenant']
	repository = params['repository']
	branch = params['branch']
	hash = params['hash']
	raise "must be selected" if host.empty? || repository.empty? || branch.empty? || hash.empty? || tenant.empty?

	content = File.read("config/hosts.yml")
	hosts = YAML.load(content)

	dist_path = "builds/#{repository}/#{branch}/#{hash}"
	host_info = hosts["hosts"][host]
	remote_dist_path = "/var/www/#{tenant}.#{host_info['domain']}/shared/#{repository}"
	`ssh -F config/config #{host} "mkdir -p #{remote_dist_path}"`
	`ssh -F config/config #{host} "rm -rf #{remote_dist_path}"`
	`scp -r -F config/config #{dist_path} #{host}:#{remote_dist_path}`

	deploy_info = {'hosts' => {}}
	File.open(deploy_info_path, 'r') do |f|
		content = f.read
		deploy_info = YAML.load(content) if content.size > 0
		deploy_info['hosts'][host] ||= { 'tenants' => {} }
		deploy_info['hosts'][host]['tenants'][tenant] ||= { 'repos' => {} }
		deploy_info['hosts'][host]['tenants'][tenant]['repos'][repository] ||= {} 
		deploy_info['hosts'][host]['tenants'][tenant]['repos'][repository] = { branch => hash }
	end
	File.open('.deploy_info.yml', 'w') do |f|
		f.write deploy_info.to_yaml
	end
        push_deploy_info("#{Time.now.strftime('%Y-%m-%d')}: deploy #{hash} of  #{branch} of #{repository} to #{tenant}.#{host}")
	'true'
end
