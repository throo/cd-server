# README #

# 環境構築方法
### デプロイ先サーバへのsshを可能にする為の設定
* ./ssh/configにgwサーバとbitbucketへの接続設定を追加
* `mkdir -p .ssh/keys/main`
* gwへの接続に使うキーペアを.ssh/keys/mainに置く
* awsのTrusted HostsグループにグローバルIPを追加
### bitbucketのリポジトリにアクセスする為の設定
* knowbuild-glassリポジトリのsetup/filesにあるgit.id_rsaとgit.id_rsa.pubを~/.sshに配置
* git config --global user.email "<your email>"
* git config --global user.name "<your name>"
### rubyのインストール
* `curl -sL https://rpm.nodesource.com/setup_8.x | sudo bash -`
* `sudo yum -y install git bzip2 gcc gcc-c++ openssl-devel readline-devel zlib-devel epel-release nodejs`
* `git clone https://github.com/rbenv/rbenv.git ~/.rbenv`
* `git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build`
* bashrcに以下を追記
```
export RBENV_ROOT=~/.rbenv
export PATH=${RBENV_ROOT}/bin:$PATH
eval "$(rbenv init --no-rehash -)"
```
* rbenv install 2.6.5

### yarnのインストール
* `curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo`
* `sudo yum install -y yarn`
### cd-sererのセットアップ
* cd ~
* git clone git@bitbucket.org:throo/cd-server.git
* cd cd-server
* gem install bundler
* bundle install

### nginxのセットアップ
* `sudo yum install -y nginx`
* 以下の内容を/etc/nginx/conf.d/default.confに記述
```
server {
   listen 8080 default_server;
    location / {
      proxy_set_header    Host                $http_host;
      proxy_set_header    X-Real-IP           $remote_addr;
      proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
      proxy_set_header    X-Forwarded-Proto   $scheme;
      proxy_set_header    X-Accel-Mapping     /rails/files/=/private_files/;
      proxy_pass http://localhost:4567;
      auth_basic "Restricted";
      auth_basic_user_file /etc/nginx/conf.d/.htpasswd;
    }
}
```
* `sudo service nginx start`
* `sudo firewall-cmd --add-port=8080 --permanent`
* `sudo firewall-cmd --reload`
* これで8080番ポートで待ち受けるようになる

# 設定ファイル
4つの設定ファイルが存在する
### config/hosts.yml
* デプロイ先サーバの情報を書く
### config/build.yml
* 定期ビルドの対象リポジトリ及び対象ブランチを指定する
### config/config
* デプロイ先サーバのssh接続情報を書く
* ローカルにあるconfigから適当に抜き出して追記する
### config/schedule.rb
* ビルド周期を記述する


## 動かし方
* cd-serverディレクトリに入る
* bundle exec whenever --update-cron
* ruby server.rb
