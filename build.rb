require 'fileutils'
require 'yaml'

CD_SERVER_PATH = "#{Dir.home}/cd-server"

def ch_root
  Dir.chdir(CD_SERVER_PATH)
end

def init
  Dir.mkdir('repos') unless Dir.glob('repos').any? 
  Dir.mkdir('builds') unless Dir.glob('builds').any?
end

def clone(repository_name, repository)
  Dir.chdir('repos')
  if Dir.glob(repository_name).any? 
    Dir.chdir('..')
    return
  end
  # reviwe ディレクトリ名を指定してclone
  `git clone #{repository['url']}`
  Dir.chdir('..')
end

def pull(repository_name, branch_name)
  `cd repos/#{repository_name} && git checkout . && git checkout #{branch_name} && git pull`
end

def build(repository_name, branch_name)
  Dir.chdir('builds')
  Dir.mkdir(repository_name) unless Dir.glob(repository_name).any?
  Dir.chdir(repository_name)
  Dir.mkdir(branch_name) unless Dir.glob(branch_name).any?

  ch_root
  Dir.chdir("repos/#{repository_name}")
  `git checkout #{branch_name}`
  hash = `git rev-parse --short HEAD`.chomp
  ch_root
  Dir.chdir("builds/#{repository_name}/#{branch_name}")
  unless Dir.glob(hash).any?
    ch_root
    Dir.chdir("repos/#{repository_name}")
    puts "current branch of #{repository_name}: " + `git rev-parse --abbrev-ref HEAD`
    # knowbuild特有の処理を切り出して、汎用的にする
    `yarn install`
    `rm -rf node_modules/videojs-seek-buttons/node_modules/video.js`
    `yarn build && mv dist ../../builds/#{repository_name}/#{branch_name}/#{hash}`
  end
  ch_root
end

ch_root
init
pid_filename = ".build.running"
build_config_filename = "config/build.yml"
return if Dir.glob(pid_filename).any?

begin
  FileUtils.touch(pid_filename)
  content =  File.read(build_config_filename)
  config = YAML.load(content)
  repositories = config['repos']
  repositories.keys.each do |repository_name|
    repository = repositories[repository_name]
    clone(repository_name,repository)
    repository['branches'].each do |branch_name|
      puts "#{repository_name}, #{branch_name}"
      pull(repository_name, branch_name)
      build(repository_name, branch_name)
    end
  end
  File.delete(pid_filename)
rescue => e
  puts e
  ch_root
  File.delete(pid_filename)
end
