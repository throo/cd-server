## ファイル構成
``````
├── Gemfile
├── Gemfile.lock
├── README.md
├── build.rb
├── config
│   ├── build.yml
│   ├── config
│   ├── hosts.yml
│   ├── id_rsa
│   ├── id_rsa.pub
│   └── schedule.rb
├── docs
│   └── structure.md
├── index.html
└── server.rb 
```

* build.rb
    * clone/pullしてきたvueプロジェクトをbuildする
* server.rb
    * APIを通じて現在のデプロイ状況の取得やデプロイを行う。server.rbはそれらのAPIを提供する
* config/build.yml
    *　buildしたいリポジトリとブランチを指定する
* config/hosts.yml
    * デプロイ先の情報を書く
* config/config
    * デプロイ先のssh情報を書く
* config/id_rsa, config/id_rsa.pub
    * デプロイ先にsshする為の鍵
* schedule.rb
    * build.rbを実行タイミングはwheneverで設定している
